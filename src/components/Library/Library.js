import React from 'react'
import {useSelector, useDispatch} from 'react-redux'
import { deleteBook } from '../../slices/bookSlice';



const Library = () => {

    const dispatch = useDispatch();

    const deleteBookHandler = (id) => {
        // Dispatch the delete book action from here
        dispatch(deleteBook({ id: id}));
    };

    const books = useSelector(state => state.books.books);

    const booksTable = books.map((book) => <tr>
        <td>{book.title}</td>
        <td>{book.author}</td>
        <td>{book.raiting}</td>
        <td><button className='delete' onClick={()=> deleteBookHandler(book.id)}>X</button></td>
    </tr>)
    return(
        <div className='library'>
            <h2>Library</h2>
            <table>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Raiting</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {booksTable}
                </tbody>
            </table>
        </div>
    )
}

export default Library;