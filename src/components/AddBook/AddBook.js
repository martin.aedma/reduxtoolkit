import React, {useState} from 'react'
import {useDispatch} from 'react-redux'
import { addBook } from '../../slices/bookSlice'


const AddBook = () => {
    const [title, setTitle] = useState('');
    const [author, setAuthor] = useState('');
    const [raiting, setRaiting] = useState('5');

    const dispatch = useDispatch();

    const addBookHandler = (e) => {
        e.preventDefault();
        // Dispatch the action for adding book
        dispatch(addBook({
            title,
            author,
            raiting,           
        }))
    }

    return (
        <form className='add-book'>
            <div>
                <label htmlFor="title">Title</label>
                <input name="title" value={title} onChange={(e) => setTitle(e.target.value)}></input>
            </div>
            <div>
                <label htmlFor='author'>Author</label>
                <input name='author' value={author} onChange={(e) => setAuthor(e.target.value)}></input>
            </div>
            <div>
                <label htmlFor='rating'>Raiting</label>
                <input name='raiting' value={raiting} onChange={(e)=> setRaiting(e.target.value)} type='number' min='1' max='10'></input>
            </div>
            <button onClick={addBookHandler}>Add Book</button>
        </form>        
    )
}

export default AddBook;