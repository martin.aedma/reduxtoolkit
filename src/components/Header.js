import React from 'react'
import { useDispatch } from 'react-redux'
import {logout, fetchRandomUser} from '../slices/authSlice'


const Header = () => {
   const dispatch = useDispatch();

   return (
        <div className='header'>
           <h1>Personal Library</h1>
           <button onClick={()=> dispatch(logout())} className='logout-btn'>Logout</button>
           <button onClick={()=> dispatch(fetchRandomUser())} className='user-btn'>Get User Data</button>
        </div>
       ) 
}

export default Header;