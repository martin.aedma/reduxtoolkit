import {createSlice} from '@reduxjs/toolkit'
import {nanoid} from 'nanoid'
import {logout} from './authSlice'

const initialState = {
    books: []
}

const bookSlice = createSlice({
    name : 'books',
    initialState,
    reducers : {
        addBook : {
            reducer : (state, action) => {
            state.books.push(action.payload);
            },
            prepare : (value) => {
                return {
                    payload : {
                        ...value,
                        date: new Date(),
                        id :  nanoid()
                    }
                }
            }
        },
        deleteBook(state, action) {
            state.books = state.books.filter ((book) => book.id !== action.payload.id)
        },
    },
    extraReducers: (builder) => {
        builder.addCase(logout, (state, action) => {
            state.books = [];
        })
    }
});

console.log(bookSlice);

export const {addBook, deleteBook} = bookSlice.actions

export default bookSlice.reducer;